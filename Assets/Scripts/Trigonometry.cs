﻿using UnityEngine;
using System.Collections;

public static class Trigonometry {
	public static int RoundToInt(this float number)
	{
		int floor = (int)number;
		if (number - floor < 0.5f)
			return floor;
		else
			return floor + 1;
	}
}
