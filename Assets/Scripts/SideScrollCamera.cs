﻿using UnityEngine;
using System.Collections;

public class SideScrollCamera : MonoBehaviour {

	#region attributes
	public float MinLimit = -10;
	public float MaxLimit = 10;
	public float OffSet = 0.5f;

	[SerializeField]
	Transform m_characterTransform;
	[SerializeField]
	Transform m_transform;
	#endregion

	#region methods
	void Update()
	{
		float distanceBetweenMaxLimitAndCharacter = m_characterTransform.position.x - (m_transform.position.x + CommonData.HalfWidthWorldScreen - OffSet);
		if (MaxLimit > m_transform.position.x && distanceBetweenMaxLimitAndCharacter > 0) {
			Vector3 pos = m_transform.position; 
			pos.x += distanceBetweenMaxLimitAndCharacter;
			m_transform.position = pos;
		}

		float distanceBetweenMinLimitAndCharacter = (m_transform.position.x - CommonData.HalfWidthWorldScreen + OffSet) - m_characterTransform.position.x;
		if (MinLimit < m_transform.position.x && distanceBetweenMinLimitAndCharacter > 0) {
			Vector3 pos = m_transform.position; 
			pos.x -= distanceBetweenMinLimitAndCharacter;
			m_transform.position = pos;
		}
	}
	#endregion
}
