﻿using UnityEngine;
using System.Collections;

public class CommonData : MonoBehaviour {
	#region attributes
	[SerializeField]
	Camera m_mainCamera;

	public static float HeightWorldScreen;
	public static float WidthWorldScreen;
	public static float HalfHeightWorldScreen;
	public static float HalfWidthWorldScreen;
	public static float AspectRatio;
	#endregion

	#region methods
	void Start () {
		DontDestroyOnLoad (this);
		AspectRatio = Screen.width / (float)Screen.height;
		HalfHeightWorldScreen = m_mainCamera.orthographicSize;
		HalfWidthWorldScreen = m_mainCamera.orthographicSize * AspectRatio;
		HeightWorldScreen = HalfHeightWorldScreen * 2f;
		WidthWorldScreen = HalfWidthWorldScreen * 2f;
	}
	#endregion
}
