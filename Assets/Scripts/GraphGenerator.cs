﻿using UnityEngine;
using System.Collections;

public class GraphGenerator : MonoBehaviour {
	#region attributes
	public float TotalWidth = 32;
	public float TotalHeight = 10;
	public Vector2 initialPosition;
	public float NodeSize = 0.5f;

	public GameObject[] NodePrefab;
	public Node.NodeState ToolSelected = Node.NodeState.EMPTY;
	public Transform Transform;
	#endregion

	#region methods
	void OnDrawGizmos()
	{
		Vector2 vectorSize = new Vector2(NodeSize,NodeSize);
		float numberOfRows = (TotalWidth / NodeSize).RoundToInt();
		float numberOfCols = (TotalHeight / NodeSize).RoundToInt();
		for (int i = 0; i < numberOfRows; i++) {
			for (int j = 0; j < numberOfCols; j++) {
				Vector2 position = initialPosition;
				position.x += -TotalWidth * 0.5f + i * NodeSize;
				position.y += -TotalHeight * 0.5f + j * NodeSize;
				Gizmos.DrawWireCube (position, vectorSize);
			}
		}
	}
	#endregion
}
