﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor (typeof(GraphGenerator))]
public class GraphGeneratorEditor : Editor {

	GraphGenerator graphGenerator;

	public void OnEnable()
	{
		graphGenerator = (GraphGenerator)target;
	}

	public override void OnInspectorGUI()
	{
		base.OnInspectorGUI ();
//		GUILayout.BeginHorizontal();
//		GUILayout.Label(" Grid Total Width ");
//		GraphGenerator.TotalWidth = EditorGUILayout.FloatField(GraphGenerator.TotalWidth, GUILayout.Width(32));
//		GUILayout.EndHorizontal();

		SceneView.RepaintAll();
	}


	void OnSceneGUI()
	{
		int controlID = GUIUtility.GetControlID (FocusType.Passive);
		Event evnt = Event.current;
		Camera camera = Camera.current;
		Ray ray = camera.ScreenPointToRay (new Vector3 (evnt.mousePosition.x, -evnt.mousePosition.y + camera.pixelHeight, 0));
		Vector3 mousePosition = ray.origin;

		if (evnt.isMouse && evnt.type == EventType.MouseDown) {
			GUIUtility.hotControl = controlID;
			evnt.Use ();
			if (graphGenerator.NodePrefab != null && graphGenerator.NodePrefab.Length > (int)graphGenerator.ToolSelected) {
				Node node = GameObject.Instantiate (graphGenerator.NodePrefab[(int)graphGenerator.ToolSelected]).GetComponent<Node>();
				Vector2 nodePosition = new Vector2 (
					Mathf.Floor(mousePosition.x / graphGenerator.NodeSize) * graphGenerator.NodeSize + graphGenerator.NodeSize * 0.5f, 
					Mathf.Floor(mousePosition.y / graphGenerator.NodeSize) * graphGenerator.NodeSize + graphGenerator.NodeSize * 0.5f);

				node.Transform.position = nodePosition;
				node.Transform.SetParent (graphGenerator.Transform);
			}
		}

		if (evnt.isMouse && evnt.type == EventType.MouseUp) {
			GUIUtility.hotControl = 0;
		}
	}
}
