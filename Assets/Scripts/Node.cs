﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Node : MonoBehaviour {

	#region ATTRIBUTES
	public enum NodeState
	{
		EMPTY,
		JUMP,
		FILLED
	}
	private NodeState _state;

	public Vector2 position;
	public Node parent; //need for graph
	public List<Node> neighbours;
	public bool isFilled
	{
		get { return _state == NodeState.FILLED;}
	}
	public bool needJump
	{
		get { return _state == NodeState.JUMP;}
	}

	public Transform Transform;
	public SpriteRenderer spriteRenderer;
	#endregion

	#region METHODS
	// Use this for initialization
	void Awake () {
		neighbours = new List<Node> ();
		SetState(NodeState.EMPTY);
	}

	public void SetState(NodeState state)
	{
		_state = state;
		switch(state)
		{
		case NodeState.EMPTY:
			spriteRenderer.color = Color.blue;
			break;
		case NodeState.JUMP:
			spriteRenderer.color = Color.green;
			break;
		case NodeState.FILLED:
			spriteRenderer.color = Color.red;
			break;
		}
	}
	public NodeState GetState()
	{
		return _state;
	}
	#endregion
}
