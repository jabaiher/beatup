﻿using UnityEngine;
using System.Collections;

public class CharacterBehaviourController : Character {

	#region attributes
	public KeyCode KeyUp;
	public KeyCode KeyDown;
	public KeyCode KeyLeft;
	public KeyCode KeyRight;
	public KeyCode KeyJump;
	public KeyCode KeyAttack;
	#endregion

	#region methods
	protected override void Update()
	{
		if (Input.GetKeyDown (KeyJump))
			Jump ();
		base.Update ();
		Vector3 direction = GetDirectionMovement ();
		if (direction != Vector3.zero)
			Move (direction);
	}

	/// <summary>
	/// Gets the direction of the movement based on the keys pressed
	/// </summary>
	/// <returns>The direction movement.</returns>
	Vector3 GetDirectionMovement()
	{
		Vector3 direction = Vector3.zero;
		if (Input.GetKey (KeyUp) && !Input.GetKey (KeyDown))
			direction.z = 1;
		else if (!Input.GetKey (KeyUp) && Input.GetKey (KeyDown))
			direction.z = -1;

		if (!Input.GetKey (KeyLeft) && Input.GetKey (KeyRight))
			direction.x = 1;
		else if (Input.GetKey (KeyLeft) && !Input.GetKey (KeyRight))
			direction.x = -1;

		direction = direction.normalized * Speed * Time.deltaTime;

		return direction;
	}
	#endregion
}
