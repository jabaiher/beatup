﻿using UnityEngine;
using System.Collections;

public class Character : MonoBehaviour {

	#region attributes
	//reference parameters
	[SerializeField]
	Rigidbody2D m_characterRigidBody;
	[SerializeField]
	Transform m_characterBodyTransform;
	[SerializeField]
	Transform m_characterShadowTransform;
	[SerializeField]
	GameObject m_gameObject;

	//speed parameters
	public float Speed = 3.5f;

	//jump parameters
	public float JumpPower = 2;
	public float JumpTime = 0.5f;
	public float FallTime = 0.5f;
	protected bool m_jumping = false;
	protected bool m_falling = false;
	float m_jumpStartTime;
	float m_fallStartTime;
	[SerializeField]
	AnimationCurve JumpCurve;
	[SerializeField]
	AnimationCurve FallCurve;

	const int characterLayerID = 8;
	const int characterAirLayerID = 9;
	#endregion

	#region methods
	protected virtual void Update()
	{
		if (m_jumping) {
			float percent = (Time.time - m_jumpStartTime) / JumpTime;
			if (percent >= 1) {
				m_jumping = false;
				m_falling = true;
				m_fallStartTime = Time.time + (Time.time - m_jumpStartTime) - JumpTime;
				percent = 1;
			}
			float damping = JumpCurve.Evaluate (percent);
			Move (new Vector3(0,JumpPower * damping * Time.deltaTime, 0));
		}
		else if(m_falling)
		{
			float percent = (Time.time - m_fallStartTime) / FallTime;
			float damping = FallCurve.Evaluate (percent);
			Move (new Vector3(0,-JumpPower * damping * Time.deltaTime, 0));
			if (percent >= 1) {
				m_falling = false;
				percent = 1;
				ResetJumpPosition ();
			}
		}
	}

	/// <summary>
	/// Move the character into specified direction.
	/// NOTE: Uses x and z to move the body and shadow of the character and y to move only the body
	/// </summary>
	/// <param name="direction">Direction.</param>
	protected void Move(Vector3 direction)
	{
		Vector3 position = m_characterRigidBody.position;
		position += new Vector3 (direction.x, direction.z, 0);
		position.z = position.y;
		m_characterRigidBody.MovePosition(position);
		if(direction.y != 0)
			m_characterBodyTransform.localPosition += new Vector3 (0, direction.y, 0);
	}

	protected void ResetJumpPosition()
	{
		m_characterBodyTransform.localPosition = new Vector3 (0, 0, 0);
		m_gameObject.layer = characterLayerID;
	}

	/// <summary>
	/// Make the character jump.
	/// </summary>
	protected void Jump()
	{
		if (!m_jumping && !m_falling) {
			m_jumping = true;
			m_jumpStartTime = Time.time;
			m_gameObject.layer = characterAirLayerID;
		}
	}
	#endregion
}